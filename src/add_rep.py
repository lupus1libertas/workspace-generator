#!/usr/bin/env python3
import os
import shutil
import sys
# import subprocess
import errno, os, stat, shutil
import urllib.request
# import urllib.request
import base64
import json

# USERNAME = input('Enter bitbucket username: ')#'lupus1libertas'
# USER_EMAIL = USERNAME + '@gmail.com'
# PASSWORD = input('Enter password: ')
API_URL = 'https://api.bitbucket.org/2.0'

def set_rw(operation, name, exc):
    os.chmod(name, stat.S_IWRITE)
    return True
# shutil.rmtree('path', onerror=set_rw)


def delete_this_script():
    # own_path = os.path.dirname(os.path.realpath(__file__))
    # own_path = os.path.join(own_path, __file__)
    # print(own_path)
    own_path = sys.argv[0]
    os.remove(own_path)
    # subprocess.Popen('''python -c "import os, time; time.sleep(1); os.remove('{}');"'''.format(own_path))


def download_folders_structure():
    os.system('git init')
    os.system('git remote add origin https://lupus1libertas@bitbucket.org/lupus1libertas/workspace-generator.git')
    os.system('git pull origin master')
    # shutil.rmtree('.git')
    shutil.rmtree('.git', onerror=set_rw)


def connect_to_repository(repository_address):
    os.system('git remote add origin {}'.format(repository_address))
    os.system('git fetch origin')
    os.system('git merge --allow-unrelated-histories origin/master')


def init_workspace_folder():
    download_folders_structure()
    create_local_repository()
    

def create_local_repository():
    os.system('git init')
    os.system('git add --all')
    os.system('git commit -m "autoupdate"')

def init_src_folder():
    pass


def create_remote_repository(username, password):    
    repository_name = input('Enter repository name.\nYou need use not existing name in your schema ')
    url = '{}/repositories/{}/{}'.format(
        API_URL, 
        username, 
        repository_name.lower().replace(' ', '-')
    )
    credentials = base64.b64encode("{0}:{1}".format(username, password).encode()).decode("ascii")
    headers = {'Authorization': "Basic " + credentials}
    data = urllib.parse.urlencode({"name": repository_name, "is_private": str(True)}).encode()
    request = urllib.request.Request(url=url, headers=headers, data=data, method="POST")

    result_url = None
    try:
        connection = urllib.request.urlopen(request)
        try:
            str_decoded = connection.read().decode("utf-8", "strict")
            content = json.loads(str_decoded)
            result_url = content['links']['clone'][0]['href']
        except Exception as e:
            print('Error', e.__dict__)
        connection.close()
        print('Repository created')
    except urllib.error.HTTPError as e:
        print('{}:\nCode: {}, Server_msg: {}'.format('Request  not performed', e.code, e.msg))
    
    # use for testing
    # request = urllib.request.Request(url=url, headers=headers, data=data, method="DELETE")
    # try:
    #     connection = urllib.request.urlopen(request)
    #     connection.close()
    #     print('Repository Deleted')
    #     # return url
    # except urllib.error.HTTPError as e:
    #     print('{}:\nCode: {}, Server_msg: {}'.format('Request  not performed', e.code, e.msg))
    #     return None
    
    print('git clone address: ', result_url)
    return result_url


def clone_repository(repository_address):
    os.system('git clone {}'.format(repository_address))


def main():
    repository_address = None
    command = input('Is repository based on new or existing workspace?\n1/2:\n')

    if command == '1':
        username = input('Enter bitbucket username: ')
        password = input('Enter password: ')
        repository_address = create_remote_repository(username, password)
    elif command == '2':
        repository_address = input('Enter workspace repository adress: ')
    
    # init_workspace_folder()
    
    if repository_address:     
        clone_repository(repository_address)
    
    # init_src_folder()

    # delete_this_script()
    input('\nComplete. If yoy have new files in workspace folder, do git push\n')


if __name__ == "__main__":
    main()
